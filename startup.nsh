# -*- Mode: NSIS; -*-

# See the readme.org file!

echo -off

# Add the default launch option here, it must be defined below
# set -v defaultarg linux
# set -v defaultarg windows

#If no args, use default arg
if /s x%1 eq x then
    echo "No arguments were given, using default %defaultarg%"
    startup.nsh %defaultarg%
endif

set -v findarg startup.nsh
set -v return ESP_FIND_RET
goto FIND
:ESP_FIND_RET
set -v esp %findres%

# ADD ENTRIES HERE:

## Match on the parameter passed to the startup.nsh script from the command line, or the default arg, can use any boolean expression
## path is the path to the EFI application e.g linux kernel, chainloaded bootloader
## args are passed to the kernel as EFI application arguments.
## Initrd doesn't have %esp%, since it's a parameter pased to the kernel

## Examples:

# if /s %1 eq gentoo then
#     set -v path %esp%\EFI\gentoo\std\bootx64.efi
#     set -v args "root=/dev/sda3 rootfstype=btrfs rootflags=acl,autodefrag,commit=25,barrier,compress=no,nodatacow,nodatasum,nodiscard,inode_cache,noflushoncommit,space_cache=v2,nossd rw resume=/dev/sda2 net.ifnames=1 mem_encrypt=off processor.ignore_ppc=1 loglevel=7 splash"
#     set -v initrd \EFI\gentoo\initrd.img-5.9.0-gentoo.img
#     goto LAUNCH
# endif
#
# if /s %1 eq windows then
#     set -v path %esp%\EFI\Microsoft\bootx64.efi
#     goto LAUNCH
# endif

set -v findarg %1
set -v return SCRIPT_FIND_RET
goto FIND
:SCRIPT_FIND_RET
if /s x%findres% ne x then
    %findres%\%1 >v path
    goto LAUNCH
else
    echo "Script %1 not found at top level."
    goto ENDSCRIPT
endif

:FIND
set -v mappings 0
map -t hd FS* >v mappings
for %a in %mappings%
    for %b run (0 10)
        if /s FS%b: eq %a then
            if exist FS%b:\%findarg% then
                set -v findres FS%b:
                goto %return%
            endif
        endif
    endfor
endfor
goto %return%

:LAUNCH
echo "Launching %path% %args% %initrd%"
if x%initrd% ne x then
    %esp%
    %path% %args% initrd=%initrd%
else
    %path% %args%
endif
echo "Failed to launch %path%"

:ENDSCRIPT
echo -on
echo "Exiting script..."
